package com.tourApplication.service;


import com.tourApplication.dto.ReservationRequest;
import com.tourApplication.model.Client;
import com.tourApplication.model.Reservation;
import com.tourApplication.model.Tour;
import com.tourApplication.repo.ClientRepository;
import com.tourApplication.repo.ReservationRepository;
import com.tourApplication.repo.TourRepository;
import lombok.Data;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Data
public class ReservationServiceImpl implements ReservationService {

    @Autowired
    private final TourRepository tourRepository;

    @Autowired
    private final ClientRepository clientRepository;

    @Autowired
    private final ReservationRepository reservationRepository;


    @Override
    public Reservation bookTour(ReservationRequest request) {

        Long tourId = request.getTourId();
        Tour tour = tourRepository.findById(tourId)
                .orElseThrow(() -> new IllegalArgumentException("There is no such a Tour with an ID: " + tourId));

        Client client = new Client();
        client.setFirstName(request.getClientFirstName());
        client.setLastName(request.getClientLastName());
        client.setEmail(request.getClientEmail());
        client.setPhone(request.getClientPhone());
        val savedClient = clientRepository.save(client);

        Reservation reservation = new Reservation();
        reservation.setCheckedIn(false);
        reservation.setClient(savedClient);
        reservation.setTour(tour);

        return reservationRepository.save(reservation);
    }



}
