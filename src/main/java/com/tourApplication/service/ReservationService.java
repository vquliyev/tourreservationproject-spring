package com.tourApplication.service;

import com.tourApplication.dto.ReservationRequest;
import com.tourApplication.model.Reservation;

public interface ReservationService {

    Reservation bookTour(ReservationRequest request);

}
