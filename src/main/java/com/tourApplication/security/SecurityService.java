package com.tourApplication.security;

public interface SecurityService {

    boolean login(String username, String password);

}
