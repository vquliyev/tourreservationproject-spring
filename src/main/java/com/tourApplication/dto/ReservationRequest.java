package com.tourApplication.dto;

import lombok.Data;

@Data
public class ReservationRequest {

    private Long tourId;
    private String clientFirstName;
    private String clientLastName;
    private String clientEmail;
    private String clientPhone;

    public ReservationRequest(String clientFirstName, String clientLastName, String clientEmail, String clientPhone) {
        this.clientFirstName = clientFirstName;
        this.clientLastName = clientLastName;
        this.clientEmail = clientEmail;
        this.clientPhone = clientPhone;
    }
}
