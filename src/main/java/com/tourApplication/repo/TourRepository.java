package com.tourApplication.repo;

import com.tourApplication.model.Tour;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface TourRepository extends JpaRepository<Tour, Long> {

    @Query("from Tour where departureDate=:departureDate and arrivalDate=:arrivalDate")
    List<Tour> findTours(@Param("departureDate") Date departureDate, @Param("arrivalDate") Date arrivalDate);

}
