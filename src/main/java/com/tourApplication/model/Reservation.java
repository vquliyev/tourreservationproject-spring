package com.tourApplication.model;


import lombok.*;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
public class Reservation extends AbstractEntity {

    private boolean checkedIn;
    @OneToOne
    private Client client;
    @OneToOne
    private Tour tour;

}
