package com.tourApplication.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.PrePersist;
import java.util.Date;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
public class Tour extends AbstractEntity {

    private String description;
    private Date departureDate;
    private Date arrivalDate;
    private String price;
    private Date createdDate;
    @PrePersist
    void createdDate() {
        this.createdDate = new Date();
    }

}
