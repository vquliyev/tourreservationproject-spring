package com.tourApplication.model;


import lombok.*;

import javax.persistence.Entity;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
public class Client extends AbstractEntity {

    private String firstName;
    private String lastName;
    private String email;
    private String phone;

}
