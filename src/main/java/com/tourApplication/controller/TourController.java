package com.tourApplication.controller;

import com.tourApplication.model.Tour;
import com.tourApplication.repo.TourRepository;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@Controller
@CrossOrigin
@Data
public class TourController {

    @Autowired
    private final TourRepository tourRepository;

    @RequestMapping("/createTour")
    public String showCreateTourPage() {
        return "createTourPage";
    }

    @RequestMapping(value = "createTour", method = RequestMethod.POST)
    public String createTour(@ModelAttribute("tour") Tour tour) {
        tourRepository.save(tour);
        return ("adminPage");
    }

    @RequestMapping("/deleteTour/{tourId}")
    public String deleteTour(@PathVariable("tourId") Long tourId) {
        tourRepository.deleteById(tourId);
        return "deletedSuccessMsg";
    }

    @RequestMapping("displayAllTours")
    public String displayAllTours(ModelMap modelMap) {
        List<Tour> allTours = tourRepository.findAll();
        modelMap.addAttribute("allTours", allTours);
        return "allToursPage";
    }

    @RequestMapping("user/displayAllTours")
    public String displayAllToursForUser(ModelMap modelMap) {
        List<Tour> allTours = tourRepository.findAll();
        modelMap.addAttribute("allTours", allTours);
        return "allToursPageForUser";
    }

    @RequestMapping("findTours")
    public String findTours(@RequestParam("departureDate") @DateTimeFormat(pattern = "dd-MMM-yyyy") Date departureDate,
                            @RequestParam("arrivalDate") @DateTimeFormat(pattern = "dd-MMM-yyyy") Date arrivalDate,
                              ModelMap modelMap) {

        List<Tour> tours = tourRepository.findTours(departureDate, arrivalDate);
        modelMap.addAttribute("tours", tours);

        return "displayTours";
    }

}
