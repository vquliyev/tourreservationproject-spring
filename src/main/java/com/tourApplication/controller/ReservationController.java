package com.tourApplication.controller;


import com.tourApplication.dto.ReservationRequest;
import com.tourApplication.model.Reservation;
import com.tourApplication.model.Tour;
import com.tourApplication.repo.TourRepository;
import com.tourApplication.service.ReservationService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

@Controller
@CrossOrigin
@Data
public class ReservationController {

    @Autowired
    private final TourRepository tourRepository;

    @Autowired
    private final ReservationService reservationService;

    @RequestMapping("/showCompleteReservation/{tourId}")
    public String showCompleteReservation(@PathVariable("tourId") Long tourId, ModelMap modelMap) {
        Tour tour = tourRepository.findById(tourId)
                .orElseThrow(() -> new IllegalArgumentException("There is no such a Tour with an ID: " + tourId));
        modelMap.addAttribute("tour", tour);
        return "completeReservation";
    }

    @RequestMapping(value = "/completeReservation/{tourId}", method = RequestMethod.POST)
    public String completeReservation(ReservationRequest request, @PathVariable("tourId") Long tourId, ModelMap modelMap) {
        request.setTourId(tourId);
        Reservation reservation = reservationService.bookTour(request);
        modelMap.addAttribute("msg", "Tour created successfully and the ID is " + reservation.getId());
        return "reservationConfirmation";
    }


}
