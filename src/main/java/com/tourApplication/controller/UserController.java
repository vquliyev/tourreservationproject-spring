package com.tourApplication.controller;

import com.tourApplication.model.User;
import com.tourApplication.repo.UserRepository;
import com.tourApplication.security.SecurityService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;


@Controller
@CrossOrigin
@Data
public class UserController {

    @Autowired
    private final UserRepository userRepository;

    @Autowired
    private final BCryptPasswordEncoder encoder;

    @Autowired
    private final SecurityService securityService;

    @RequestMapping("/welcome")
    public String welcomePage() {
        return "index";
    }

    @RequestMapping("/signUp")
    public String showRegistrationPage() {
        return "signUpPage";
    }

    @RequestMapping(value = "addUser", method = RequestMethod.POST)
    public String addUser(@ModelAttribute("user") User user) {
        user.setPassword(encoder.encode(user.getPassword()));
        userRepository.save(user);
        return "login";
    }

    @RequestMapping("/login")
    public String showLoginPage() {
        return "login";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login(@RequestParam("username") String username, @RequestParam("password") String password, ModelMap modelMap) {
        //User user = userRepository.findByUsername(username);
        boolean loginResult = securityService.login(username, password);
//        if (user.getPassword().equals(password)) {
//            return "landingPage";
//        } else {
//            modelMap.addAttribute("msg", "Invalid User Name or Password. Please try again");
//        }
        if (loginResult) {
            return "landingPage";
        } else {
            modelMap.addAttribute("msg", "Invalid User Name or Password. Please try again");
        }
        return "login";
    }

    @RequestMapping("/admin-login")
    public String showAdminPage() {
        return "adminPage";
    }

}
